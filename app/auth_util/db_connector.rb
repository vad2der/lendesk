class DbConnector
  class << self
    def get_record(namespace, record_id)
      $redis.get("#{namespace}:#{record_id}")
    end

    def set_record(namespace, record_id, record_body)
      $redis.set("#{namespace}:#{record_id}", record_body)
    end
  end
end
