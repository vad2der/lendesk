require 'db_connector'

class AuthUtil
  PASSWORD_REQUIREMENTS = /\A(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[[:^alnum:]])/x.freeze
  # DbConnector = DbConnector
  @namespace = 'auth_util'

  class << self
    attr_reader :db_connector

    def password_valid?(password)
      # at least 1 lowercase, 1 uppercase, 1 numnber, 1 special symbol, overall length min 8 symbols
      password.match(PASSWORD_REQUIREMENTS)
    end

    def check_username_unique(username)
      !JSON.parse(DbConnector.get_record(@namespace, 'all_names')).include? username
    end

    def authenticate(username, password)
      user = JSON.parse(DbConnector.get_record(@namespace, username))
      salt = user["salt"]
      text_to_encrypt = "#{salt}#{password}"
      encrypted_password = encrypt_one_direction(text_to_encrypt)
      encrypted_password.eql? user["encrypted_password"]
    end

    def create_account(username, password)
      # 1 generate salt
      salt = SecureRandom.base64(12)

      # 2 hash salt + password
      text_to_encrypt = "#{salt}#{password}"
      encrypted_password = encrypt_one_direction(text_to_encrypt)
      auth_body = { salt: salt, encrypted_password: encrypted_password }.to_json
      DbConnector.set_record(@namespace, username, auth_body)
      add_username_to_list(username)
    end

    def get_user(username)
      DbConnector.get_record(@namespace, username)
    end

    def encrypt_one_direction(text)
      Digest::MD5.hexdigest(text)
    end

    def add_username_to_list(username)
      # let's create a record with all names, quering all data and mapping names would be not as efficient
      all_usernames = DbConnector.get_record(@namespace, 'all_names')
      if all_usernames.nil?
        DbConnector.set_record(@namespace, 'all_names', [username])
      else
        JSON.parse(all_usernames) << username
        DbConnector.set_record(@namespace, 'all_names', all_usernames)
      end
    end
  end
end
