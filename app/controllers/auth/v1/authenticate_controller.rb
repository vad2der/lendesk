class Auth::V1::AuthenticateController < ApplicationController
  before_action :set_vars

  def create
    puts @username, @password
    authentication = AuthUtil.authenticate(@username, @password)
    if authentication
      render(json: { message: 'Authentication has been successful', success: true }, status: :ok)
    else
      render(json: { mesasge: 'Authentication failed', success: false }, status: :unauthorized)
    end
  rescue StandardError => e # TODO: make it error type specific
    # TODO: log the error
    puts e
    render(json: { message: 'Unexpected error happen', success: false }, status: :internal_server_error)
  end

  private

  def authenticate_params
    params.permit(:username, :password, authenticate: %i[username password])
  end

  def set_vars
    @username = authenticate_params[:username]
    @password = authenticate_params[:password]
  end
end
