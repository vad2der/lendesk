class Auth::V1::CreateLoginController < ApplicationController
  before_action :check_username_unique, :validate_password

  def create
    AuthUtil.create_account(@username, @password)
    render(json: { message: 'User account has been created', success: true }, status: :ok)
  rescue StandardError => e # TODO: make it error type specific
    # TODO: log the error
    puts e
    render(json: { message: 'Unexpected error happen', success: false }, status: :internal_server_error)
  end

  private

  def check_username_unique
    @username = create_login_params[:username]
    username_unique = AuthUtil.check_username_unique(@username)
    render(json: { message: 'supplied username already exists', success: false }, status: :unprocessable_entity) unless username_unique
  end

  def validate_password
    @password = create_login_params[:password]
    password_valid = AuthUtil.password_valid?(@password)
    render(json: { message: 'supplied password is invalid', success: false }, status: :expectation_failed) unless password_valid
  end

  def create_login_params
    params.permit(:username, :password, create_login: %i[username password])
  end
end
