require 'test_helper'

class Auth::V1::AuthenticateControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get auth_v1_authenticate_create_url
    assert_response :success
  end
end
