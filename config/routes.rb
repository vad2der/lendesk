Rails.application.routes.draw do
  namespace :auth do
    namespace :v1 do
      resources :create_login, only: [:create]
      resources :authenticate, only: [:create]
    end
  end
end
