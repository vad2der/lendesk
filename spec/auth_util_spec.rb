require './test/test_helper'

RSpec.describe AuthUtil, :auth_util_test do
  describe '#password_valid?' do
    it 'returns nil for text without a single lowercase' do
      expect(described_class.password_valid?('12!@AS56HJ')).to be(nil)
    end

    it 'returns nil for text without a single uppercase' do
      expect(described_class.password_valid?('12!@fg56as')).to be(nil)
    end

    it 'returns nil for text without a single number' do
      expect(described_class.password_valid?('tr!@ASsdHJ')).to be(nil)
    end

    it 'returns nil for text without a sspecial char' do
      expect(described_class.password_valid?('12asAS56HJ')).to be(nil)
    end

    it 'returns nil for text shorter then 8' do
      expect(described_class.password_valid?('12!@Af')).to be(nil)
    end

    it 'returns not nil for proper text' do
      expect(described_class.password_valid?('12!@as56HJ')).not_to be(nil)
    end
  end

  describe "#check_username_unique" do
    # mock the DbConnector service
    before do
      allow(DbConnector).to receive(:get_record) do |_namespace, record_id|
        %w[name1 name2].to_json if record_id == 'all_names'
      end
    end

    it "name exists" do
      expect(described_class.check_username_unique('name1')).to be(false)
    end

    it "name does not exist" do
      expect(described_class.check_username_unique('name3')).to be(true)
    end
  end

  describe "#authenticate" do
    let(:user_record) { { salt: 'DlNhVssNoWI7HLAO', encrypted_password: '454f6e1068a479f33ff65f5cdb146318' }.to_json }
    let(:password) { 'so&^67sdFDS!' }
    let(:wrong_password) { 'so&^67sdsfsd' }
    let(:username) { 'name1' }

    # mock the DbConnector service
    before do
      allow(DbConnector).to receive(:get_record) do |_namespace, record_id|
        user_record if record_id == username
      end
    end

    it "authenticates user with correct username & password" do
      expect(described_class.authenticate(username, password)).to be(true)
    end

    it "returns nil for user with wrong username & password" do
      expect(described_class.authenticate(username, wrong_password)).to be(false)
    end
  end

  describe "creates account" do
    let(:password) { 'so&^67sdFDS!' }
    let(:wrong_password) { 'so&^67sdsfsd' }
    let(:username) { 'name1' }
    let(:usernames) { %w[name3 name2] }
    let(:usernames_plus) { %w[name3 name2 name1] }

    # mock the DbConnector service
    before do
      allow(DbConnector).to receive(:set_record) do |_namespace, record_id, _body|
        usernames_plus.to_json if record_id == 'all_names'
        "OK"
      end

      allow(DbConnector).to receive(:get_record) do |_namespace, record_id|
        usernames.to_json if record_id == 'all_names'
      end
    end

    it "creates user account" do
      expect(described_class.create_account(username, password)).to eql("OK")
    end
  end

  describe "gets user" do
    let(:user_record) { { salt: 'DlNhVssNoWI7HLAO', encrypted_password: '454f6e1068a479f33ff65f5cdb146318' }.to_json }
    let(:username) { 'name1' }

    # mock the DbConnector service
    before do
      allow(DbConnector).to receive(:get_record) do |_namespace, record_id|
        user_record if record_id == username
      end
    end

    it "gets the user by name" do
      expect(described_class.get_user('name1')).to eq(user_record)
    end

    it "gets nil for unknown the user by name" do
      expect(described_class.get_user('name2')).to eq(nil)
    end
  end

  describe "does one direction encription" do
    let(:salt) { 'sOme^*salT' }
    let(:password) { 'Som%Pa55w0$0' }
    let(:text_to_test) { `#{salt}#{password}` }

    it "consistently hashes the same input to the same output" do
      hash1 = described_class.encrypt_one_direction(text_to_test)
      hash2 = described_class.encrypt_one_direction(text_to_test)
      expect(hash1).to eq(hash2)
    end
  end
end
