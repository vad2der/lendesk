# README

## Task
Using Ruby on Rails as your framework and Redis for data storage, we need an authentication API for internal services to create and authenticate users. This API should be RESTful and use JSON. It should be fast and secure, and be able to pass a basic security audit (e.g. password complexity). If there are areas of security that your solution hasn't had time to address they should be annotated for future development.

The API should be able to create a new login with a username and password, ensuring that usernames are unique. It should also be able to authenticate this login at a separate end point. It should respond with 200 OK messages for correct requests, and 401 for failing authentication requests. It should do proper error checking, with error responses in a JSON response body.

## Instructions

* Ruby version 2.7

* System dependencies
  
  - Need to have Redis installed

* Configuration
  
  - `bundle`

* Database creation
  
  - According to requirements it hosted on Redis

* Database initialization
  
  - `redis-server`

* How to run the test suite
  
  - run `rspec`
  - to ensure linting run `rubocop`
  - to check HTTP calls, open [postman collection](Lendesk.postman_collection.json)

## REST API

  ### create_login
  method = **POST**
  required params: *username, password*

  <!-- responses to be listed-->

  ### authenticate
  method = **POST**
  required params: *username, password*

  <!-- responses to be listed-->
  <!-- parms to be moved to headers -->